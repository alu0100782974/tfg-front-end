import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { Client } from '../../pojo/clientPojo';
import { Location } from '../../pojo/locationPojo'
import { Observable } from 'rxjs/Rx';
import { truckIcon, flagIcon, blackIcon, servedIcon } from '../utils/markers';
import 'rxjs/add/operator/map';
import { Service } from '../../pojo/servicePojo';
import { ClientsServedProvider } from '../../providers/clients-served/clients-served';
import { LocationsProvider } from '../../providers/locations/locations';
import { NextClientProvider } from '../../providers/prueba/prueba';

declare var L: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //Map
  @ViewChild('map') mapContainer: ElementRef;
  map: any;

  //Websocket
  private ws;

  public connected = false;

  public nextClient: Client = new Client();

  //JSON that contains the markers inserted into the map (except act.pos)
  markers = [];

  //Actual position
  public actualPos = { lat: 0, lon: 0 };

  //Controllers
  controllers = [];

  //Markers
  public truck: any;

  //Button controls
  public connect: boolean = false;
  public show: boolean = true;
  public start: boolean = true;
  public next: boolean = true;
  public serve: boolean = true;
  public served: boolean = true;
  public end: boolean = true;

  private truckId: number;
  private serverId;
  private port1;
  private port2;

  private backendUrl: string;


  constructor(
    public navCtrl: NavController,
    private navParams: NavParams,
    private clientsServedProvider: ClientsServedProvider,
    private locationsProvider: LocationsProvider,
    private nextClientProvider: NextClientProvider) {

    this.truckId = this.navParams.get('userId');
    this.serverId = this.navParams.get('serverId');
    this.port1 = this.navParams.get('port1');
    this.port2 = this.navParams.get('port2');

    this.backendUrl = `http://${this.serverId}:${this.port2}`;
    this.clientsServedProvider.setUrlBackend(this.backendUrl);
    this.clientsServedProvider.setTruckId(this.truckId);
    this.locationsProvider.setUrlBackend(this.backendUrl);
    this.locationsProvider.setTruckId(this.truckId);
    this.nextClientProvider.setUrlBackend(this.backendUrl);

  }

  ionViewDidEnter() {
    sessionStorage.clear();
    this.loadmap();
  }

  ///////////////////////////////////////////////////////////
  ////////////////////// CREATE MAP /////////////////////////
  ///////////////////////////////////////////////////////////

  center: [number, number] = [28.4091675, -16.5616061]

  public loadmap() {

    this.map = L.map("map", {
      center: this.center,
      zoom: 10,
      maxZoom: 11,
      minZoom: 11,
      zoomAnimationThreshold: 10,
      zoomControl: false
    });

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'MyMap',
    }).addTo(this.map);

    this.actualPos.lat = this.center[0];
    this.actualPos.lon = this.center[1];
    this.truck = L.marker(this.center, { icon: truckIcon, zIndexOffset: 999999 });

    this.truck.addTo(this.map);
  }

  ///////////////////////////////////////////////////////////
  ///////////////////// PARSE JSON //////////////////////////
  ///////////////////////////////////////////////////////////

  private clients = [];
  private clientsaux = [];
  private clientsToServe = [];

  public generatePointsFromJson(truckId) {

    this.cleanMap();

    this.clients = [];
    this.clientsToServe = [];

    if (sessionStorage.getItem != null) {
      this.clients = JSON.parse(sessionStorage.getItem("clients"));

      //Introducing actual position to the route
      this.clientsToServe.push({ lat: this.actualPos.lat, lon: this.actualPos.lon })

      if (this.clients.length > 0) {
        for (var i = 0; i < this.clients.length; i++) {
          if ((this.clients[i].truck_id == truckId) && (this.clients[i].served === "no")) {
            var aux = {
              id: this.clients[i].id,
              lat: this.clients[i].coordinates.lat,
              lon: this.clients[i].coordinates.lon,
              open: this.clients[i].open,
              close: this.clients[i].close
            }
            this.clientsToServe.push(aux);
          }
        }
      }
    }
    this.printRoute();
  }

  private microPoints = [];

  async printRoute() {

    let control;
    let waypointaux = [];
    const prom: Promise<any[]>[] = [];
    let aux = {};

    for (let i = 0; i < this.clientsToServe.length - 1; i++) {

      waypointaux = [
        { lat: this.clientsToServe[i].lat, lon: this.clientsToServe[i].lon },
        { lat: this.clientsToServe[i + 1].lat, lon: this.clientsToServe[i + 1].lon }
      ];


      control = L.Routing.control({
        waypoints: waypointaux,
        routeWhileDragging: false,
        autoRoute: true,
        useZoomParameter: false,
        draggableWaypoints: false,
        show: false,
        addWaypoints: false,
        lineOptions: {
          styles: [{ color: 'blue', opacity: 1, weight: 5 }],
        }
      })

      prom.push(
        new Promise((resolve, reject) => {
          control.on('routeselected', (e) => {
            aux[i] = e.route.coordinates;
            resolve();
          });
        })
      );
      control.addTo(this.map);
      this.controllers.push(control);
    }
    Promise.all(prom).then((e) => {
      for (let i = 0; i < this.clientsToServe.length - 1; i++) {
        this.microPoints[i] = aux[i];
      }
    });
    new L.marker(this.center, { icon: flagIcon, zIndexOffset: 999999 }).addTo(this.map);
    this.checkPoints();

  }

  ///////////////////////////////////////////////////////////
  ///////////////// CONNECTION BUTTON ///////////////////////
  ///////////////////////////////////////////////////////////

  public connectToServer() {
    sessionStorage.clear();
    new Promise((resolve, reject) => {
      this.connectToWebsocket(resolve, reject);
    }).then((res) => {
      this.connect = true;
      this.show = false;
    }).catch((err) => {
      alert(err)
    });
  }

  ///////////////////////////////////////////////////////////
  ///////////////// SHOW ROUTE BUTTON ///////////////////////
  ///////////////////////////////////////////////////////////

  public showMyRoute() {
    if (this.ws.readyState == 1) {
      this.show = true;
      this.sendMessage();
      this.generatePointsFromJson(this.truckId);
      this.start = false;
    } else {
      alert('Server is down, please restart the app');
    }
  }

  ///////////////////////////////////////////////////////////
  ///////////////// START ROUTE BUTTON //////////////////////
  ///////////////////////////////////////////////////////////


  public timeStart;
  private actualCli = 0;
  public locationInterval;
  public timerInterval;
  public timer = 0;

  public startRoute() {

    this.start = true;
    this.next = false;
    this.connected = true;
    this.timeStart = new Date();

    this.locationInterval = setInterval(() => {

      let aux: Location = {
        truckId: this.truckId,
        lat: this.actualPos.lat,
        lon: this.actualPos.lon
      }

      this.locationsProvider.saveLocation(aux);

    }, 2000)

    this.timerInterval = setInterval(() => {

      this.timer++;

    }, 1000)

    this.nextClientProvider.getNextClient(this.clientsToServe[this.actualCli + 1].id).subscribe(client => {
      this.nextClient = client;
    });

    this.calculateTotalDistance();
  }


  ///////////////////////////////////////////////////////////
  ///////////////// TO NEXT CLIENT BUTTON ///////////////////
  ///////////////////////////////////////////////////////////


  public goToNextClient() {

    this.next = true;

    this.map.panTo(new L.latLng(this.actualPos.lat, this.actualPos.lon));

    if (this.ws.readyState == 1) {

      //this.sendMessage();
      //this.generatePointsFromJson(this.truckId);

      this.moveMarker(this.actualCli);


    } else {
      alert('Server is down, please restart the app');
    }

  }

  ///////////////////////////////////////////////////////////
  /////////////////////// SERVING BUTTON ///////////////////
  ///////////////////////////////////////////////////////////

  public serveInterval = null;
  public timeStartService;

  public startService() {

    this.timeStartService = new Date();

    this.timeStartService = new Date();
    //this.timeStartService.setHours(7);

    if ((this.clientsToServe[this.actualCli + 1].open > this.timeStartService.getHours()) || (this.clientsToServe[this.actualCli + 1].close < this.timeStartService.getHours())) {
      alert('Client is closed, please wait until it opens');

      clearInterval(this.serveInterval);
      this.serveInterval = setInterval(() => {

        this.sendMessage();
        this.generatePointsFromJson(this.truckId);
        this.map.panTo(new L.latLng(this.actualPos.lat, this.actualPos.lon));

      }, 5000)
    } else {
      if (this.serveInterval != null) {
        clearInterval(this.serveInterval);
      }
      this.serve = true;
      this.served = false;
    }

  }

  ///////////////////////////////////////////////////////////
  ///////////////// CLIENT SERVED BUTTON ///////////////////
  ///////////////////////////////////////////////////////////

  public timeEndService;

  public endService() {

    //poner marcadores 
    this.timeEndService = new Date();
    this.served = true;
    this.actualCli++;

    let aux: Service = {
      truckId: this.truckId,
      clientId: this.clientsToServe[this.actualCli].id,
      startService: this.timeStartService,
      endService: this.timeEndService
    }

    this.clientsServedProvider.saveClientInfo(aux);

    if (this.actualCli == this.clientsToServe.length - 1) {
      this.end = false;
    } else {
      this.next = false;
      this.nextClientProvider.getNextClient(this.clientsToServe[this.actualCli + 1].id).subscribe(client => {
        this.nextClient = client;
      });
    }

    let auxMarker = new L.marker(new L.latLng(this.actualPos.lat, this.actualPos.lon), {
      icon: servedIcon,
      zIndexOffset: 99999
    }).addTo(this.map);

    //this.cleanMap();

    //this.markers.push(auxMarker);

  }

  ///////////////////////////////////////////////////////////
  ///////////////// END SIMULATION BUTTON ///////////////////
  ///////////////////////////////////////////////////////////

  public timeEnd;

  public endSimulation() {

    clearInterval(this.locationInterval);
    clearInterval(this.timerInterval);

    this.end = true;

    this.timeEnd = new Date();

    alert('Route finished');
  }

  ///////////////////////////////////////////////////////////
  //////////////// WEBSOCKET FUNCTIONS //////////////////////
  ///////////////////////////////////////////////////////////

  public sendMessage() {
    if (this.ws.readyState == 1) {
      this.ws.send(JSON.stringify('enviado'));
    }
  }

  public connectToWebsocket(resolve, reject) {

    this.ws = new WebSocket(`ws://${this.serverId}:${this.port1}`);

    this.ws.onopen = function (e) {
      console.log('Connection to server opened');
      resolve();
    }

    this.ws.onmessage = function (e) {
      var data = JSON.parse(e.data);
      this.clients = JSON.stringify(data);
      sessionStorage.setItem("clients", this.clients);
    }

    this.ws.onclose = function (e) {
      console.log("Connection closed");
      clearInterval(this.locationInterval);
      reject('Connection not established, please try it again');
    }

    this.ws.onerror = function (err) {
      console.log(`Error en el WebSocket: ${err}`);
    };
  }

  ///////////////////////////////////////////////////////////
  //////////////// AUXILIAR FUNCTIONS ///////////////////////
  ///////////////////////////////////////////////////////////

  public cleanMap() {
    if (this.markers != null) {
      for (var i = 0; i < this.markers.length; i++) {
        this.map.removeLayer(this.markers[i]);
      }
    }

    if (this.controllers != null) {
      for (var j = 0; j < this.controllers.length; j++) {
        this.map.removeControl(this.controllers[j]);
      }
    }
  }

  public actualDistance: number = 0;

  public moveMarker(index) {

    let time = 0;
    let control = 0;

    for (let i = 0; i < this.microPoints[index].length; i++) {
      setTimeout(() => {

        let auxLat = this.actualPos.lat;
        let auxLon = this.actualPos.lon;

        this.map.removeLayer(this.truck);

        this.actualPos.lat = this.microPoints[index][i].lat;
        this.actualPos.lon = this.microPoints[index][i].lng;

        this.actualDistance += Math.trunc(this.getDistance(
          [auxLat, auxLon],
          [this.actualPos.lat, this.actualPos.lon]
        ));

        this.truck = new L.marker(new L.latLng(this.microPoints[index][i].lat, this.microPoints[index][i].lng), {
          icon: truckIcon,
          zIndexOffset: 9999999
        }).addTo(this.map);

        control++;
        if (control == (this.microPoints[index].length - 1)) {
          this.serve = false;
        }
        this.map.panTo(new L.latLng(this.microPoints[index][i].lat, this.microPoints[index][i].lng));
      }, time);
      time += 20;
    }
  }

  public checkPoints() {

    let dateAux = new Date();
    console.log(dateAux);
    //dateAux.setHours(7);

    for (let i = 0; i < this.clientsToServe.length; i++) {

      if ((this.clientsToServe[i].open > dateAux.getHours()) || (this.clientsToServe[i].close < dateAux.getHours())) {
        var aux = new L.marker(new L.latLng(this.clientsToServe[i].lat, this.clientsToServe[i].lon), {
          icon: blackIcon
        }).addTo(this.map);
        this.markers.push(aux);
      }
    }
  }

  private getDistance(origin, destination) {

    var lon1 = this.toRadian(origin[1]),
      lat1 = this.toRadian(origin[0]),
      lon2 = this.toRadian(destination[1]),
      lat2 = this.toRadian(destination[0]);

    var deltaLat = lat2 - lat1;
    var deltaLon = lon2 - lon1;

    var a = Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon / 2), 2);
    var c = 2 * Math.asin(Math.sqrt(a));
    var EARTH_RADIUS = 6371;
    return c * EARTH_RADIUS * 1000;
  }

  private toRadian(degree) {
    return degree * Math.PI / 180;
  }

  public totalDistance: number;

  public calculateTotalDistance() {
    this.totalDistance = 0;
    console.log(this.microPoints);
    if (this.microPoints.length != 0) {
      for (var i = 0; i < this.microPoints.length; i++) {
        for (var j = 0; j < this.microPoints[i].length - 1; j++) {
          this.totalDistance += Math.trunc(this.getDistance(
            [this.microPoints[i][j].lat, this.microPoints[i][j].lng],
            [this.microPoints[i][j + 1].lat, this.microPoints[i][j + 1].lng]
          ));
        }
      }
    }
    this.totalDistance = this.totalDistance;
  }


  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////

}
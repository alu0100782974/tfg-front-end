import { Component } from '@angular/core';
import { ClientsServedProvider } from '../../providers/clients-served/clients-served';
import { Service } from '../../pojo/servicePojo';
import { LocationsProvider } from '../../providers/locations/locations';
import { Location } from '../../pojo/locationPojo';

/**
 * Generated class for the MenuComponent component.‽
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'menu',
  templateUrl: 'menu.html'
})
export class MenuComponent {

  public services: Service[] = [];
  public locations: Location[] = [];

  public display = false;
  public display2 = false;

  constructor(
    private clientServedProvider: ClientsServedProvider,
    private locationProvider: LocationsProvider) { }

  public getClientsServed(truckId: number) {
    this.display2 = false;
    this.display = !this.display;

    this.clientServedProvider.services
      .subscribe(servicesResponse => {
        this.services = servicesResponse;
      });
  }

  public getLocations(truckId: number) {
    this.display = false;
    this.display2 = !this.display2;

    this.locationProvider.getLocations().subscribe(loc => {
      this.locations = loc;
    });
  }

}

import { Injectable } from '@angular/core';
import { Service } from '../../pojo/servicePojo';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { Http } from '@angular/http';
import { Client } from '../../pojo/clientPojo';

@Injectable()
export class NextClientProvider {

  public urlBackend;
  /* public nextClient: BehaviorSubject<Client[]>; */

  constructor(public http: Http) {
  }

  public getNextClient(clientId: number): Observable<Client> {
    /* this.nextClient.next([]); */
    return this.http.get(`${this.urlBackend}/clients/id=${clientId}`).map(res => res.json());
  }

  public setUrlBackend(server: string): void {
    this.urlBackend = server;
  }

  /* public get getClient(): Observable<Client[]> {
    return this.nextClient.filter(data => !!data);
  } */

}
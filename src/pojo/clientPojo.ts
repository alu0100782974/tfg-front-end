export class Client {
    public id: number;
    public name: string;
    public address: string;
    public truckId: number;
    public open: number;
    public close: number;
    public served: string;
    public closed: string;

    constructor() {
        this.id = undefined;
        this.name = undefined;
        this.address = undefined;
        this.truckId = undefined;
    }
}